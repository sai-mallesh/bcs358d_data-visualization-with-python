#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 30 02:17:24 2023

@author: putta
"""

# from bokeh.plotting import figure, show

# x = [1, 2, 3, 4, 5]
# y1 = [6, 7, 2, 4, 5]
# y2 = [2, 3, 4, 5, 6]
# y3 = [4, 5, 5, 7, 2]

# # create a new plot with a title and axis labels
# p = figure(title="Multiple line example", x_axis_label="x", y_axis_label="y")

# # add multiple renderers
# p.line(x, y1, legend_label="Temp.", color="blue", line_width=2)
# p.line(x, y2, legend_label="Rate", color="red", line_width=2)
# p.line(x, y3, legend_label="Objects", color="green", line_width=2)

# # show the results
# show(p)

import numpy as np

from bokeh.layouts import gridplot
from bokeh.plotting import figure, show

x = np.linspace(0, 4*np.pi, 100)
y = np.sin(x)

TOOLS = "pan,wheel_zoom,box_zoom,reset,save,box_select"

p1 = figure(title="Example 1", tools=TOOLS)

p1.circle(x,   y, legend_label="sin(x)")
p1.circle(x, 2*y, legend_label="2*sin(x)", color="orange")
p1.circle(x, 3*y, legend_label="3*sin(x)", color="green")

p1.legend.title = 'Markers'

p2 = figure(title="Example 2", tools=TOOLS)

p2.circle(x, y, legend_label="sin(x)")
p2.line(x, y, legend_label="sin(x)")

p2.line(x, 2*y, legend_label="2*sin(x)",
        line_dash=(4, 4), line_color="orange", line_width=2)

p2.square(x, 3*y, legend_label="3*sin(x)", fill_color=None, line_color="green")
p2.line(x, 3*y, legend_label="3*sin(x)", line_color="green")

p2.legend.title = 'Lines'

show(gridplot([p1, p2], ncols=2, width=400, height=400))